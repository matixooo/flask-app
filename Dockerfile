FROM alpine:3.14.3

LABEL maintainer="Matixoooo"

RUN mkdir -p /home/appuser/app

COPY ./flask-project /home/appuser/app

RUN apk update --no-cache && apk upgrade -U -a --no-cache && apk add --no-cache python3 py-pip curl && ln -sf python3 /usr/bin/python \
 && python -m pip install -U flask

RUN addgroup -S appgroup && adduser -S appuser -G appgroup

WORKDIR /home/appuser/app

USER appuser

EXPOSE 5000

ENTRYPOINT ["./bootstrap.sh"]

from flask import Flask, jsonify, request, render_template
import os

#myhost = os.uname()[1]

app = Flask(__name__, template_folder='template')

@app.route("/")
def id():
  #return myhost
  return render_template('index.html', myhost = os.uname()[1])

@app.route("/isAlive")
def hc():
#  status_code = flask.Response(status=200)
  return 'IS OK'
